import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Route, Routes, BrowserRouter} from "react-router-dom";

import Header from './header';
import Login from './Login'
import Signup from './Signup'

import { RenderMap } from './mapStuff';
import NewDass from './NewDass'
import { NewReview } from './AddReview'
import { DassInfo } from './DassInfo';
import { MyReviews } from './my-reviews';
import Search from "./search";
import { RequireAuth } from './wigdets'

//@ts-ignore
import styles from "./style.css"
import 'bootstrap/dist/css/bootstrap.min.css';
import "../node_modules/mazemap/mazemap.min.css"

const rootElement = document.createElement("div");
rootElement.className=styles.pageRoot;
if (document.body) {
    document.body.appendChild(rootElement);
} else {
    throw new Error("No body found");
}

ReactDOM.render(<div>Loading...</div>, rootElement);



window.addEventListener('load', () => {

ReactDOM.render(
    //RenderMap wont render properly if it is inside a container like <div>. It has to be at root level.
  <>
            <BrowserRouter>
            <Header/>
                <Routes>
                    <Route  path="/" element={<RenderMap/>}  />
                    <Route  path="/login" element={<Login from="/"/>}  />
                    <Route  path="/reviews/*" element={<RequireAuth><NewReview/></RequireAuth>} />
                    <Route  path="/signup" element={<Signup/>}  />
                    <Route  path="/newToilet" element={<RequireAuth > <NewDass/></RequireAuth>}/>
                    <Route  path="/toilets/*" element={<DassInfo/>}  />
                    <Route path="/search/*" element={<Search searchString=""/>} />
                    <Route path="/myReviews" element={<RequireAuth><MyReviews/></RequireAuth>} />
                </Routes>
            </BrowserRouter>
    </>,
    rootElement);

});
