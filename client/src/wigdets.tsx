import React, {Children} from 'react';
import { Navigate } from "react-router-dom"
import Login from "./Login"
import { getCookie } from "./utils"
interface AlertProps {
    message?: string;
    type?: "danger" | "success";
}

export class Alert extends React.Component<AlertProps>{
    render(){
        return(
            <div className={"alert alert-"+ (this.props.type || "danger") } role="error">
                <p>{this.props.message}</p> 
            </div>
        )
    }
}
export class RequireAuth extends React.Component{
    render(){
        console.log("pathname",  window.location.pathname)

        return(
            <>
                {getCookie("jwt")?this.props.children:<Login needAuth={true} from={window.location.pathname + window.location.search}/>}
            </>
        )
    }
    
}
