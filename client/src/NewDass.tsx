import * as React from 'react';
import { Alert } from './wigdets'
import { ToiletMap } from './ToiletMap';
import { getCookie } from './utils'
import { Card, InputGroup, Col, Container, FormControl, Button, Form , Dropdown, Row } from 'react-bootstrap';
let url: string = "http://localhost:3001/api/v1"
type Test ={
    name : string;
    description : string;
    poi_id: number | undefined
    alert: string | undefined;
    campus_id: number | undefined;
    gender: number;
    image_link: string | undefined;
}
type Data ={
    name : string;
    description : string;
    poi_id: number | undefined
    campus_id: number | undefined;
    gender: number;
    image_link: string | undefined;
}
export default class NewDass extends React.Component<any, Test>{
    type: "danger" | "success"
    state :Test = {
        name: "",
        description: "",
        poi_id: undefined,
        alert: undefined,
        campus_id: 1,
        gender: 0, //default is 0, corresponding to everyone. This is because the state does not update before you tap the selected tag
        image_link: undefined
    }
    constructor(props: any){
        super(props)
        this.type = "danger"
    }
    render(){
        return(
            <>
                <Container>
                    <Row>
                    <Col>
                        <Card style={{height: "50vh", margin: "1rem", padding:"1rem"}}>
                            {this.state.alert ? <Alert type={this.type} message={this.state.alert}/>:""}
                            <Card.Title>New Toilet</Card.Title>
                            <InputGroup className="mb-3"> 
                                <InputGroup.Text id="InputGroup-sizing-default">Toilet Name</InputGroup.Text>
                                    <FormControl
                                      aria-label="Default"
                                      aria-describedby="inputGroup-sizing-default"
                                      onChange={(e) => (this.setState(({name:e.target.value})))}
                                    />

                            </InputGroup> 
                            <InputGroup className="mb-3"> 
                                <InputGroup.Text id="InputGroup-sizing-default">Description</InputGroup.Text>
                                    <FormControl
                                        as="textarea"
                                      aria-label="Default"
                                      aria-describedby="inputGroup-sizing-default"
                                      onChange={(e) => (this.setState({description:e.target.value}))}
                                    />
                            </InputGroup> 
                            
                            <InputGroup className="mb-3"> 
                                <InputGroup.Text id="InputGroup-sizing-default">Image link</InputGroup.Text>
                                    <FormControl
                                      aria-label="Default"
                                      aria-describedby="inputGroup-sizing-default"
                                      onChange={(e) => (this.setState(({image_link:e.target.value})))}
                                    />

                              </InputGroup>
                              <InputGroup className="mb-3">
                                <InputGroup.Text id="InputGroup-sizing-default">Type</InputGroup.Text>
                                  <Form.Select onChange={e => (this.setState({gender: Number(e.target.value)}))}>
                                <option value="0">Everyone</option>
                                <option value="1">Male</option>
                                <option value="2">Women</option>
                                <option value="3">Handicaped</option>
                                <option value="4">Other</option>
                            </Form.Select>

                            </InputGroup> 
                                 <InputGroup className="mb-3">
                                    <Button variant="primary" onClick={this.save.bind(this)}>
                                      Save
                                    </Button>
                                  </InputGroup>
                        </Card>
                        </Col>
                        <Col>
                                <h3>Select a location</h3>
                            {/* bind the update function so it can work on object variables from another component */}
                            <ToiletMap updatePoi={this.updatePoi.bind(this)}/>  
                        </Col>
                    </Row>
                </Container>
            </>
        )
    }
    componentDidMount(){
        /* console.log("storage", localStorage.getItem("jwt")) */
        /* if(!localStorage.getItem("jwt")){ */
        /*     window.location.href = "/login/?L=1" */
        /* } */
    }
    updatePoi(poi: number): void{
        this.setState({poi_id: poi})
    }

    save(){
        if(this.state.name == "" || !this.state.poi_id ){
            this.setState({alert : "You must spesify a name and a location"})
            return
        }
        if(this.state.image_link){
            console.log(this.state.image_link)
            if(!this.state.image_link?.match(/[\/.](gif|jpg|jpeg|tiff|png)$/i)){
                this.setState({alert : "Image link most point to an image file"})
                return
            }
        }
        let data: Data = {
            name: this.state.name,
            description: this.state.description,
            poi_id: this.state.poi_id,
            campus_id: this.state.campus_id,
            gender : this.state.gender,
            image_link: this.state.image_link

        }
        let token = getCookie("jwt")
        if(!token){
            this.setState({alert : "You need to be logged in"})
            return
        }
        fetch(url + "/toilets", {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            /* credentials: 'include', */
            /* mode: 'no-cors', // no-cors, *cors, same-origin */
            headers: {
                'Content-Type': 'application/json',
                "Authorization" : `${token}`
            },
            body: JSON.stringify(data) // body data type must match "Content-Type" header
        })
            .then(response => {
                if(response.status != 201){
                    this.setState({alert : "Could not create toilet"})
                }
                else{
                    response.text()
                    .then((data) =>{
                        this.type = "success"
                        this.setState({alert :data})
                        this.type = "danger"
                    })
                    .catch(() => {
                        this.setState({alert : "Something went wrong"})
                    })
                }
            })
            .catch(() => {
                this.setState({alert : "Could not connect to the database"})
            })
    }
}
