import React from 'react';
import Container from 'react-bootstrap/Container';
import { Button, Form, FormControl, Navbar } from 'react-bootstrap';
import Nav from 'react-bootstrap/Nav';
import { Routes, Route, Outlet, Link } from "react-router-dom";
import { getCookie } from "./utils"
import Search from './search';

type myState = {
    loggedIn: boolean;
    searchString: string;
}

export class Header extends React.Component<any, myState> {
    state: myState = {
        loggedIn : false,
        searchString: ""
    }
    render() {
        return (
            <Navbar bg="dark" variant="dark">
                <Container>
                    <a href="/" style={{textDecoration: "none"}}><Navbar.Brand >myDass</Navbar.Brand></a>
                    <Nav className="justify-content-end">
                        <Link to="/newToilet"><Button variant="outline-primary" >New toilet</Button></Link>
                        {this.state.loggedIn ? <Link to="/"> <Button variant="outline-danger" onClick={this.logout.bind(this)}>Logout</Button></Link>:<Link to="/login"><Button variant="outline-success">Login</Button></Link>}
                    </Nav>
                    <Link to="/myReviews"> <Button variant="outline-primary">myReviews</Button></Link>
                    <Form className="d-flex">
                        <FormControl
                        type="search"
                        placeholder="Search"
                        className="me-2"
                        aria-label="Search"
                        onChange={e => {
                            this.setState({
                                searchString: e.target.value.toLowerCase()
                            })
                        }}
                        />
                        <Link to={"/search/?q="+this.state.searchString}><Button variant="outline-success" >Search</Button></Link>
                    </Form>
                </Container>
            </Navbar>
        );    
    }
    logout(){
        localStorage.removeItem("jwt")
        this.setState({loggedIn: false})
    }
    componentWillMount(){
        if(getCookie("jwt")){
            this.setState({loggedIn: true})
        }
    }
}
export default Header;
