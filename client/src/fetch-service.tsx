import { Review, Toilet } from "../../types";
import { getCookie } from "./utils";

let url: string = "http://localhost:3001/api/v1";
let token = getCookie("jwt");

export async function getToilets() {
    let response = await fetch(`${url}/toilets/`, {
        method: "GET",
        credentials: "include",
        mode: "cors",
        headers: {
            "Content-Type": "application/json",
        }
    });
    let data: Toilet[] = await response.json();
    return data;
}

export async function getToilet(id: string) {
    const response = await fetch(url + "/toilets/" + id, {
        method: 'GET',
        credentials: 'include',
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json'
            // 'Content-Type': 'application/x-www-form-urlencoded', 
        },
    });
    const data = await response.json();
    return data;
}

export async function getToiletReviews(id: string) {
    const response = await fetch(url + "/reviews/toilet/" + id, {
        method: 'GET',
        credentials: 'include',
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json'
            // 'Content-Type': 'application/x-www-form-urlencoded', 
        },
    });
    if (response.status === 404) {
        throw new Error("404");
    } else {
        const data: Review[] = await response.json();
        return data;
    }
}
// Get all votes for a review.
export async function getReviewVotes(reviewId: number | undefined) {
    const response = await fetch(url + "/reviews/" + reviewId + "/votes", {
        method: 'GET',
        credentials: 'include',
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json'
            // 'Content-Type': 'application/x-www-form-urlencoded',
        },
    });
    const data:[] = await response.json();
    return data;
}

export async function upvoteReview(reviewId: number | undefined) {
    const response = await fetch(url + "/reviewVotes/" + reviewId + "/up", {
        method: 'POST', // *GET, POST, PUT, DELETE, etc. 
        credentials: 'include',
        mode: 'cors', // no-cors, *cors, same-origin *1/ 
        headers: { 
            'Content-Type': 'application/json',
            'Authorization': `${token}`
            // 'Content-Type': 'application/x-www-form-urlencoded', 
        },
    });
    const data = await response.json();
    return data;
}