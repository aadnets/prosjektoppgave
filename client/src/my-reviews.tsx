import * as React from 'react';
import StarRatings from 'react-star-ratings';
import { Alert, Card, CardGroup, Carousel, Col, Container, Form, FormControl, Image, InputGroup, ListGroup, ListGroupItem, Pagination, Row, Button } from 'react-bootstrap';
import { getCookie } from './utils'

let url: string = "http://localhost:3001/api/v1";
import { Toilet, Review, Vote } from '../../types'

type MyProps = {};
type MyState = {
    user_id: any,
    reviews:Review[],
    review:Review | undefined,
    error: boolean, 
    errorMsg: string,
    loading: boolean,
    title: string,
    description: string,
    reviewRating: number | undefined,
    toilet_rating: number | undefined,
};

export class MyReviews extends React.Component<MyProps, MyState>{
    constructor(props: MyProps){
        super(props);
        this.state = {
            user_id: undefined,
            title: "",
            description: "",
            reviewRating: undefined,
            reviews: [],
            review: undefined,
            error: false,
            errorMsg: "",
            loading: true,
            toilet_rating: 0,
        }
    }
    async componentDidMount(){
        let token = getCookie("jwt");
        await fetch(url + "/user", {
            method: 'GET',
            credentials: 'include',
            mode: 'cors',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `${token}`
            }
        }).then(response => response.json())
        .then((data) => {
            // let object = data;
            console.log(data.user_id)
            this.setState({user_id: data.user_id})
            
        }).then(() => {
        //Fetch reviews belonging to user
        fetch(url + "/reviews/user/"+ this.state.user_id, {
            method: 'GET', // *GET, POST, PUT, DELETE, etc. 
            credentials: 'include',
            mode: 'cors', // no-cors, *cors, same-origin *1/ 
            headers: { 
                'Content-Type': 'application/json' 
                // 'Content-Type': 'application/x-www-form-urlencoded', 
            }, 
        }) 
        .then(response => response.json())
        .then(response => {
            console.log(response);
            this.setState({
                reviews: response,
                error: false,
                loading: false
            })
            //this.props.reviews = response;
        })})
    }
    editReview(){
        let token = getCookie("jwt");
        let newReview: Review;
        if(this.state.review){
            newReview= this.state.review;
            if(this.state.title.length > 0){
                newReview.title = this.state.title;
            }
            if(this.state.description.length > 0){
                newReview.description = this.state.description;
            }
            if(this.state.review?.toilet_rating != this.state.toilet_rating){
                newReview.toilet_rating = this.state.toilet_rating || 0;
            }
            console.log(newReview);
            fetch(url + "/reviews/"+ newReview.id, {
                method: 'PUT', // *GET, POST, PUT, DELETE, etc. 
                credentials: 'include',
                mode: 'cors', // no-cors, *cors, same-origin *1/ 
                headers: { 
                    'Content-Type': 'application/json',
                    'Authorization': `${token}`
                },
                body: JSON.stringify(newReview),
            }).catch(err => {
                console.error("Insufficient permissions");
            })
            this.componentDidMount();
        }
    }
    deleteReview(review:Review){
        let token = getCookie("jwt");
        fetch(url+"/reviews/"+ review.id, {
            method: 'DELETE',
            credentials: 'include',
            mode: 'cors',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `${token}`
            }
        }).catch(err => {
            console.error("Insufficient permissions");
        })
        this.componentDidMount();
    }
    onStarClick(nextValue: number) {
        this.setState({toilet_rating: nextValue});
        }
    render(){
        return(
            <div>
                <Container fluid>
                <Row >
                {this.state.error ? <Alert variant="danger">{this.state.errorMsg}</Alert>:""}
                <Col>

                <Card border="primary" bg="dark" text="white" style={{ width: '100%'}}>
                    <Card.Body>
                        <Card.Title>{this.state.review?.title || <em>No review selected</em>}</Card.Title>
                        <Card.Text>Toilet ID: {this.state.review?.id}</Card.Text>
                        <Card.Text>Review: {this.state.review?.description}</Card.Text>
                        <InputGroup className="mb-3"> 
                                <InputGroup.Text id="InputGroup-sizing-default">Title</InputGroup.Text>
                                    <FormControl
                                      aria-label="Default"
                                      aria-describedby="inputGroup-sizing-default"
                                      placeholder={this.state.review?.title}
                                      onChange={(e) => (this.setState(({title: e.target.value})))}
                                    />

                            </InputGroup> 
                            <InputGroup className="mb-3"> 
                                <InputGroup.Text id="InputGroup-sizing-default">Description</InputGroup.Text>
                                    <FormControl
                                        as="textarea"
                                      aria-label="Default"
                                      aria-describedby="inputGroup-sizing-default"
                                      placeholder={this.state.review?.description}
                                      onChange={(e) => (this.setState({description:e.target.value}))}
                                    />
                            </InputGroup>
                            <StarRatings
                        rating={this.state.toilet_rating || 0}
                        starDimension="30px"
                        starSpacing="3px"
                        numberOfStars={6}
                        starRatedColor="gold"
                        changeRating={this.onStarClick.bind(this)}
                    />  
                    {this.state.review &&
                        <Button variant="success" onClick={e => {this.editReview()}}>Lagre</Button>
                    }
                    </Card.Body>
                </Card>
                </Col>
                <Col md={8}>
                {this.state.reviews.map(e => {
                    return(
                    <Card border="primary" key={e.id}>
                        <Card.Header>{e.username || <em>User deleted</em>}</Card.Header>
                        <Card.Body>
                            <Card.Title>{e.title || <em>No title</em>}</Card.Title>
                            <Card.Text>{e.description || <em>No description</em>}</Card.Text>
                            <Row>
                                <Button variant="success" onClick={event => {
                                    this.setState({review: e})
                                    this.setState({toilet_rating: e.toilet_rating})
                                    }}>Edit</Button>
                                <Button variant="danger" onClick={event => {
                                    this.deleteReview(e)
                                    }}>Delete</Button>
                            </Row>
                        </Card.Body>
                    </Card>)
                })}
                </Col>
                
                </Row>
            </Container>

            </div>
        )
    }
}