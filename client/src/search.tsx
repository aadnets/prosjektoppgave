import * as React from 'react';
import { Card, InputGroup, Col, Container, FormControl, Button, Form , Dropdown, Row } from 'react-bootstrap';
import StarRatings from 'react-star-ratings';


let url: string = "http://localhost:3001/api/v1";
import { Toilet } from '../../types'
import { getToilets } from './fetch-service';

type MyProps = {
    searchString: string
 };
type MyState = { 
    toilets:Toilet[], 
    error: boolean, 
    loading: boolean,
    searchString: string
};

export default class Search extends React.Component<MyProps, MyState>{
    constructor(props: any){
        super(props);
        this.state = {
            
            toilets: [],
            error: false,
            loading: true,
            searchString: ""
        }

    }
    componentDidMount(){
        getToilets()
        .then(response => this.setState({ 
            toilets: response, 
            loading: false 
        })) 
        .catch(error => this.setState({ 
            error: true, 
            loading: false 
        }));

        let params = new URLSearchParams(document.location.search.substring(1));
        let searchTerm = params.get("q");
        console.log(searchTerm);
        if(searchTerm){
            this.setState(
                {searchString: searchTerm}
            );
            
        }
    }
    render(){
        return(
            <div>
                <Card>
                    <Row>
                        <Form className="d-flex">
                        <FormControl
                        type="search"
                        placeholder="Search"
                        className="me-2"
                        aria-label="Search"
                        value={this.state.searchString}
                        onChange={e => {
                            this.setState({
                                searchString: e.target.value.toLowerCase()
                            })
                        }}
                        />
                        <Button variant="outline-success">Search</Button>
                        </Form>
                        </Row>
                </Card>

                {this.state.toilets.filter(e => {
                    if(e.name){
                        if(e.name.toLowerCase().includes(this.state.searchString)){
                            return true;
                        }
                        else if(e.description){
                            return e.description.toLowerCase().includes(this.state.searchString);
                        }
                    }
                    else if(e.description){
                        return e.description.toLowerCase().includes(this.state.searchString);
                    }
                    else{
                        return false;
                    };
                }).map(e => {
                    return(
                        <Card key={e.id}>
                            <Row>
                                <Col><a href={"/toilets/?id="+e.id}><h2>{e.name}</h2></a></Col>
                                <Col>
                                    <StarRatings
                                        rating={e.avg_rating || 0}
                                        starDimension="30px"
                                        starSpacing="3px"
                                        numberOfStars={6}
                                        starRatedColor="gold"
                                    />
                                </Col>
                            </Row>
                            <Row><h3>{e.description}</h3></Row>
                        </Card>
                    )
                })}
            </div>
        )
    }
}