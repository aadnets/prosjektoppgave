import * as React from 'react';
import StarRatings from 'react-star-ratings';
import { Alert, Card, CardGroup, Carousel, Col, Container, Image, ListGroup, ListGroupItem, Pagination, Row } from 'react-bootstrap';
import { InputGroup, FormControl, Button, Form} from 'react-bootstrap';
import { getCookie } from './utils'

let url: string = "http://localhost:3001/api/v1";
import { Toilet, Review } from '../../types'


type Data = {
    toilet_rating: number | null
    toilet_id: number | undefined
    description: string
    title: string
}
type MyProps = { };
type ReviewProps =  {
    review: Review,
}
type ReviewsProps= { reviews: Review[] }
type ReviewsPropsPag= { reviews: Review[], onPageChange: any, }
type PaginationState = { reviews: Review[], start: number }
type MyState = { 
    toilet:Toilet, 
    loading: boolean,
    description: string,
    toilet_rating: number | null,
    title: string,
    success: undefined | string,
    error: undefined | string,
};

export class NewReview extends React.Component<MyProps, MyState> {
    constructor(props: MyProps) {
        super(props);
        this.state = {
            toilet: {
                id: undefined,
                name: undefined,
                description: undefined,
                gender: undefined,
                facilities: undefined,
                poi_id: undefined,
                campus_id: undefined,
                avg_rating: undefined,
                image_link: undefined,
                creator_user_id: undefined,
                calculated_avg: undefined,
            }, 
            title: "",
            description: "",
            success: undefined,
            error:undefined,
            loading: true,
            toilet_rating: 0
        }
    }

    componentDidMount() {
        //https://developer.mozilla.org/en-US/docs/Web/API/URLSearchParams/get
        // Får tak i id gjennom URL-parameter.
        let params = new URLSearchParams(document.location.search.substring(1));
        let id = params.get("id");
        if (id!==null) {
        fetch(url + "/toilets/" + id, {
            method: 'GET', // *GET, POST, PUT, DELETE, etc. 
            credentials: 'include',
            mode: 'cors', // no-cors, *cors, same-origin *1/ 
            headers: { 
                'Content-Type': 'application/json' 
                // 'Content-Type': 'application/x-www-form-urlencoded', 
            }, 
        }) 
        .then(response => response.json()) 
        .then(response => this.setState({ 
            toilet: response, 
            loading: false,
        })
        )
        .catch(error => this.setState({ error: error, loading: false }));

        }
    else this.setState({ error: "Invalid id."});
        
        
    }
    
    render() {

        return (
            <Container>
                <Row>
                {this.state.error ? <Alert variant="danger">{this.state.error}</Alert>:""}
                {this.state.success ? <Alert variant="success">{this.state.success}</Alert>:""}

                <Col>

                <Card border="primary" bg="dark" text="white" style={{ width: '20rem'}}>
                    <Card.Img variant="top" src={this.state.toilet.image_link} />
                    <Card.Body>
                            <InputGroup className="mb-3"> 
                                <InputGroup.Text id="InputGroup-sizing-default">Title</InputGroup.Text>
                                    <FormControl
                                      aria-label="Default"
                                      aria-describedby="inputGroup-sizing-default"
                                      onChange={(e) => (this.setState({title:e.target.value}))}
                                    />
                            </InputGroup> 
                            <InputGroup className="mb-3"> 
                                <InputGroup.Text id="InputGroup-sizing-default">Description</InputGroup.Text>
                                    <FormControl
                                        as="textarea"
                                      aria-label="Default"
                                      aria-describedby="inputGroup-sizing-default"
                                      onChange={(e) => (this.setState({description:e.target.value}))}
                                    />
                            </InputGroup> 
                            
                        
                        <StarRatings
                        rating={this.state.toilet_rating || 0}
                        starDimension="30px"
                        starSpacing="3px"
                        numberOfStars={6}
                        starRatedColor="gold"
                        changeRating={this.onStarClick.bind(this)}
                    />  
                     <InputGroup className="mb-3">
                        <Button variant="primary" onClick={this.save.bind(this)}>
                          Save
                        </Button>
                      </InputGroup>
                    </Card.Body>
                </Card>
                </Col>
                </Row>
            </Container>
        );    
    }
    onStarClick(nextValue: number) {
    this.setState({toilet_rating: nextValue});
    }

    save(){
        setTimeout(() => {
            this.setState({error: undefined, success: undefined})
        }, 2000);
        console.log("run1")
        if(!this.state.toilet_rating){
            this.setState({error : "You must give a rating"})
            return
        }
        let data: Data = {
            toilet_rating: this.state.toilet_rating,
            toilet_id: this.state.toilet.id,
            description: this.state.description,
            title: this.state.title
        }
        let token = getCookie("jwt")
        if(!token){
            this.setState({error : "You must be logged in"})
            return
        }
        console.log("run2")
        fetch(url + "/reviews", {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            /* credentials: 'include', */
            /* mode: 'cors', // no-cors, *cors, same-origin */
            headers: {
                'Content-Type': 'application/json',
                "Authorization" : `${token}`
            },
            body: JSON.stringify(data) // body data type must match "Content-Type" header
        })
            .then(response => {
                console.log(response)
                if(response.status != 201){
                    this.setState({error : "Something went wrong"})
                }
                else{
                    response.text()
                    .then((data) =>{
                        this.setState({success: "Created a review"})
                    })
                        .catch(() => {
                            this.setState({error : "Something went wrong"})
                        })
                }

            })
            .catch(() => {
                this.setState({error : "Something went wrong"})
            })
    }
 
}
