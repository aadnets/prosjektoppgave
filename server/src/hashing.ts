import * as bcrypt from 'bcrypt'

export function makeHash(password: string){
    return new Promise<string>((resolve, reject) => {
        const saltRounds = 10;
        bcrypt.hash(password, saltRounds)
        .then((hash) => resolve(hash))
        .catch((e) => reject(e));
    })
}
