import { testToilets, testReviews, testUsers, testReviewVotes, toilet_to_post, adminuser, review_to_post, testuser_to_login, testuser_to_post } from './testdata';
import { Toilet, User, Review, ReviewVotes } from '../../../types';
import { generateToken } from '../token';
import { makeHash } from '../hashing';
import dbRouter from "../dbRouter";
import db from "../db-service";
import express, { request } from "express";
import axios from "axios";
import { JwtPayload } from 'jsonwebtoken';

axios.defaults.adapter = require("axios/lib/adapters/http");
axios.defaults.baseURL = "http://localhost:3002";

let webServer: any;
beforeAll(() => {
    const app = express();
    app.use(express.json());
    app.use('/api/v1', dbRouter);
    webServer = app.listen(3002);
});
afterAll(() => webServer.close());

jest.mock("../db-service");

describe("Toilet requests", () => {
    describe("Fetch toilets", () => {
        
        /**
         * GET /toilets 
         * 100% coverage
         */
        test("Fetch all toilets GET(200 OK)", async () => {
            db.getAllToilets = jest.fn(() => Promise.resolve(testToilets));
    
            const response = await axios.get("/api/v1/toilets");
    
            expect(response.status).toEqual(200);
            expect(response.data).toEqual(testToilets);
        });
    
        test("Fetch all toilets GET (500 ERR)", async () => {
            db.getAllToilets = jest.fn(() => Promise.reject(new Error("Promise rejected")));
    
            return expect(async () => {
                await axios.get("/api/v1/toilets");
            }).rejects.toThrowError("Request failed with status code 500");
        });
    
        /**
         * GET /toilets/:id 100%
         */
        test("Fetch toilet with id: 2 GET (200 OK)", async () => {
            db.getToilet = jest.fn((id) => Promise.resolve(testToilets.filter(e => e.id == id)));
    
            const response = await axios.get("/api/v1/toilets/2");
    
            expect(response.status).toEqual(200);
            expect(response.data).toEqual(testToilets.find(e => e.id == 2));
        });
    
        test("Fetch toilet with id: 6 GET (404 ERR)", async () => {
            db.getToilet = jest.fn((id) => Promise.resolve(testToilets.filter(e => e.id == id)));
    
            return expect(async () => {
                await axios.get("/api/v1/toilets/6");
            }).rejects.toThrowError("Request failed with status code 404");
        });
    
        test("Fetch toilet with id: 2 (500 ERR)", async () => {
            db.getToilet = jest.fn((id) => Promise.reject(new Error("Promise rejected")));
    
            return expect(async () => {
                await axios.get("/api/v1/toilets/2");
            }).rejects.toThrowError("Request failed with status code 500");
        });
    
        /**
         * GET /toilets/:id/average 100%
         */
    
        test("Fetch average from toilet with id: 1 GET (200 OK)", async () => {
            // @ts-ignore toilet_rating is not undefined or null in our test data
            // !!! WARNING !!!: This function does not work if the testdata is expanded further than 2 elements with the same toilet id 😅
            db.getToiletAverage = jest.fn((id) => Promise.resolve([{"average": (testReviews.filter(e => e.toilet_id === 1).reduce(l => l.toilet_rating) / 2)}]))
    
            const response = await axios.get("/api/v1/toilets/1/average");
    
            expect(response.status).toEqual(200);
            expect(response.data).toEqual([{"average": 3}]);
        })
    
        test("Fetch average from toilet with id: 4 GET (404 ERR)", async () => {
            db.getToiletAverage = jest.fn((id) => Promise.resolve([]));
    
            return expect(async () => {
                await axios.get("/api/v1/toilets/6/average");
            }).rejects.toThrowError("Request failed with status code 404");
        })
    
        test("Fetch average from toilet with id: 1 GET (500 ERR)", async () => {
            db.getToiletAverage = jest.fn((id) => Promise.reject(new Error("Promise rejected")));
    
            return expect(async () => {
                await axios.get("/api/v1/toilets/6/average");
            }).rejects.toThrowError("Request failed with status code 500");
        })
    })
    
    /**
     * POST /toilets 
     * 100% coverage
     */
    describe("Post toilets", () => {
    
        test("Post toilet without errors POST (201 OK)", async () => {
            db.createToilet = jest.fn((toilet, user) => {
                return new Promise<void>((resolve, reject) => (resolve()));
            })
    
            let response = await axios.post("/api/v1/toilets", toilet_to_post, { headers:{ 'Content-Type': 'application/json', 'authorization': adminuser}})
            expect(response.status).toEqual(201);
    
        })
    
        test("Post toilet without toilet body (400 ERR)", async () => {
            return expect(async () => {
                await axios.post("/api/v1/toilets", {}, {headers:{'Content-Type': 'application/json','authorization': ""}})
            }).rejects.toThrowError("Request failed with status code 400");
    
        })
    
        test("Post toilet without authToken (401 ERR)", async () => {   
            return expect(async () => {
                await axios.post("/api/v1/toilets", toilet_to_post, {headers:{'Content-Type': 'application/json','authorization': ""}})
            }).rejects.toThrowError("Request failed with status code 401");
        })
    
        test("Post toilet with wrangled authToken (500 ERR)", async () => {
            return expect(async () => {
                await axios.post("/api/v1/toilets", toilet_to_post, {headers:{'Content-Type': 'application/json','authorization': "not working"}})
            }).rejects.toThrowError("Request failed with status code 500");
        })
    
        test("Post toilet with db error (500 ERR)", async () => {
            db.createToilet = jest.fn((toilet, user) => Promise.reject("Rejected promise"))
    
            return expect(async () => {
                await axios.post("/api/v1/toilets", toilet_to_post, {headers:{'Content-Type': 'application/json','authorization': adminuser}})
            }).rejects.toThrowError("Request failed with status code 500");
        })
    })
    
    /**
     * PUT /toilets/:id
     * 100% coverage
     */
    describe("Update normal toilets", () => {
        test("Put toilet (200 OK)", async () => {
            db.editToilet = jest.fn((toilet: Toilet, user: User) => {return new Promise<void>((resolve, reject) => resolve())})
    
            let response = await axios.put("/api/v1/toilets/1", toilet_to_post, { headers:{ 'Content-Type': 'application/json', 'authorization': adminuser}})
    
            expect(response.status).toEqual(200);
        })
    
        test("Put toilet without body (400 ERR)", async () => {
            return expect(async () => {
                await axios.put("/api/v1/toilets/1", {}, { headers:{ 'Content-Type': 'application/json', 'authorization': ""}})
            }).rejects.toThrowError("Request failed with status code 400");
        })
    
        test("Put toilet without authToken (401 ERR)", async () => {
            return expect(async () => {
                await axios.put("/api/v1/toilets/1", toilet_to_post, { headers:{ 'Content-Type': 'application/json', 'authorization': ""}})
            }).rejects.toThrowError("Request failed with status code 401");
        })

        test("Put toilet with wrangled authToken (500 ERR)", async () => {
            return expect(async () => {
                await axios.put("/api/v1/toilets/1", toilet_to_post, { headers:{ 'Content-Type': 'application/json', 'authorization': "not working"}})
            }).rejects.toThrowError("Request failed with status code 500");
        })
    
        test("Put toilet with db error (500 ERR)", async () => {
            db.editToilet = jest.fn((toilet: Toilet, user: User) => {return new Promise<void>((resolve, reject) => reject("Promise rejected"))})
    
            return expect(async () => {
                await axios.put("/api/v1/toilets/1", toilet_to_post, { headers:{ 'Content-Type': 'application/json', 'authorization': adminuser}})
            }).rejects.toThrowError("Request failed with status code 500");
        })
    })
    
    /**
     * DELETE /toilets/:id
     * 100% coverage
     */
    describe("Delete toilet", () => {
        test("succesfully delete toilet (200 OK)", async () => {
            db.deleteToilet = jest.fn((id: number, user: User) => {return new Promise<void>((resolve, reject) => resolve())})
        
            let response = await axios.delete("api/v1/toilets/1", { headers:{ 'Content-Type': 'application/json', 'authorization': adminuser }})
            expect(response.status).toEqual(200);
        })
    
        test("delete toilet without authToken (401 ERR)", async () => {
            return expect(async () => {
                await axios.delete("/api/v1/toilets/1", { headers:{ 'Content-Type': 'application/json', 'authorization': ""}})
            }).rejects.toThrowError("Request failed with status code 401");
        })
    
        test("delete toilet with wrangled authToken (500 ERR)", async () => {
            return expect(async () => {
                await axios.delete("/api/v1/toilets/1", { headers:{ 'Content-Type': 'application/json', 'authorization': "not working"}})
            }).rejects.toThrowError("Request failed with status code 500");
        })
    
        test("delete toilet with db error (500 ERR)", async () => {
            db.deleteToilet = jest.fn((id: number, user: User) => {return new Promise<void>((resolve, reject) => reject("Promise rejected"))})
    
            return expect(async () => {
                await axios.delete("/api/v1/toilets/1", { headers:{ 'Content-Type': 'application/json', 'authorization': adminuser}})
            }).rejects.toThrowError("Request failed with status code 500");
        })
    
    })
})

describe("Review requests", () => {

    describe("GET reviews", () => {
        /**
         * GET /reviews
         * 100% coverage
         */
        test("Fetch reviews (200 OK)", async () => {
            db.getAllReviews = jest.fn(() => Promise.resolve(testReviews));
    
            const response = await axios.get("/api/v1/reviews");
    
            expect(response.status).toEqual(200);
            expect(response.data).toEqual(testReviews);
        })

        test("Fetch reviews (500 ERR)", async () => {
            db.getAllReviews = jest.fn(() => Promise.reject(new Error("Promise rejected")));

    
            return expect(async () => {
                await axios.get("/api/v1/reviews");
            }).rejects.toThrowError("Request failed with status code 500");
        })

        /**
         * GET /reviews/toilet/:toiletId
         * 100% coverage
         */

         test("Fetch reviews from toilet with id: 1 (200 OK)", async () => {
            db.getToiletReviews = jest.fn((id: number) => Promise.resolve(testReviews.filter(e => e.toilet_id == id)));
    
            const response = await axios.get("/api/v1/reviews/toilet/1");
    
            expect(response.status).toEqual(200);
            expect(response.data).toEqual(testReviews.filter(e => e.toilet_id == 1));
        })

        test("Fetch review from toilet with id: 6 (404 ERR)", async () => {
            db.getToiletReviews = jest.fn((id: number) => Promise.resolve(testReviews.filter(e => e.toilet_id == id)));

            return expect(async () => {
                await axios.get("/api/v1/reviews/toilet/6");
            }).rejects.toThrowError("Request failed with status code 404");
        })

        test("Fetch review from toilet with id: 1 (500 ERR)", async () => {
            db.getToiletReviews = jest.fn((id: number) => Promise.reject(new Error("Promise rejected")));

            return expect(async () => {
                await axios.get("/api/v1/reviews/toilet/1");
            }).rejects.toThrowError("Request failed with status code 500");

        })

        /**
         * 
         * GET /reviews/:reviewId
         * 100% coverage
         */

        test("Fetch review with id: 1 (200 OK)", async () => {
            db.getReviewFromId = jest.fn((id: number) => Promise.resolve(testReviews.filter(e => e.id == id)));
    
            const response = await axios.get("/api/v1/reviews/1");
    
            expect(response.status).toEqual(200);
            expect(response.data).toEqual(testReviews.filter(e => e.id == 1));
        })

        test("Fetch review with id: 6 (404 ERR)", async () => {
            db.getReviewFromId = jest.fn((id: number) => Promise.resolve(testReviews.filter(e => e.id == id)));

            return expect(async () => {
                await axios.get("/api/v1/reviews/6");
            }).rejects.toThrowError("Request failed with status code 404");
        })

        test("Fetch review with id: 1 (500 ERR)", async () => {
            db.getReviewFromId = jest.fn((id: number) => Promise.reject(new Error("Promise rejected")));

            return expect(async () => {
                await axios.get("/api/v1/reviews/1");
            }).rejects.toThrowError("Request failed with status code 500");

        })

        /**
         * GET /reviews/user/:userId
         * 100% coverage
         */
         test("Fetch review from user with id: 1 (200 OK)", async () => {
            db.getUserReviews = jest.fn((id: number) => Promise.resolve(testReviews.filter(e => e.user_id == id)));
    
            const response = await axios.get("/api/v1/reviews/user/1");
    
            expect(response.status).toEqual(200);
            expect(response.data).toEqual(testReviews.filter(e => e.user_id == 1));
        })

        test("Fetch review from user with id: 6 (404 ERR)", async () => {
            db.getUserReviews = jest.fn((id: number) => Promise.resolve(testReviews.filter(e => e.user_id == id)));

            return expect(async () => {
                await axios.get("/api/v1/reviews/user/6");
            }).rejects.toThrowError("Request failed with status code 404");
        })

        test("Fetch review from user with id: 1 (500 ERR)", async () => {
            db.getUserReviews = jest.fn((id: number) => Promise.reject(new Error("Promise rejected")));

            return expect(async () => {
                await axios.get("/api/v1/reviews/user/1");
            }).rejects.toThrowError("Request failed with status code 500");
        })

        /**
         * GET /reviews/:reviewId/votes
         * 100% coverage
         */
         test("Fetch votes from review with id: 1 (200 OK)", async () => {
            db.getReviewRating = jest.fn((id: number) => Promise.resolve([{"SUM(vote)": 1}]));
    
            const response = await axios.get("/api/v1/reviews/1/votes");
    
            expect(response.status).toEqual(200);
            expect(response.data[0]["SUM(vote)"]).toEqual(1);
        })

        test("Fetch votes from review with id: 6 (404 ERR)", async () => {
            db.getReviewRating = jest.fn((id: number) => Promise.resolve([]));

            return expect(async () => {
                await axios.get("/api/v1/reviews/6/votes");
            }).rejects.toThrowError("Request failed with status code 404");
        })

        test("Fetch votes from review with id: 1 (500 ERR)", async () => {
            db.getReviewRating = jest.fn((id: number) => Promise.reject(new Error("Promise rejected")));

            return expect(async () => {
                await axios.get("/api/v1/reviews/1/votes");
            }).rejects.toThrowError("Request failed with status code 500");
        })
    })

    describe("POST reviews", () => {
        test("Post review without errors POST (201 OK)", async () => {
            /**
             * POST "/reviews"
             * 100% coverage
             */
            db.createReview = jest.fn((review, user) => {
                return new Promise<void>((resolve, reject) => (resolve()));
            })
    
            let response = await axios.post("/api/v1/reviews", review_to_post, { headers:{ 'Content-Type': 'application/json', 'authorization': adminuser}})
            expect(response.status).toEqual(201);
    
        })
    
        test("Post review without toilet id (400 ERR)", async () => {
            return expect(async () => {
                // we need an authtoken to get to the right if statement
                await axios.post("/api/v1/reviews", {}, {headers:{'Content-Type': 'application/json','authorization': adminuser}})
            }).rejects.toThrowError("Request failed with status code 400");
    
        })
    
        test("Post review without authToken (401 ERR)", async () => {   
            return expect(async () => {
                await axios.post("/api/v1/reviews", review_to_post, {headers:{'Content-Type': 'application/json','authorization': ""}})
            }).rejects.toThrowError("Request failed with status code 401");
        })
    
        test("Post review with wrangled authToken (500 ERR)", async () => {
            return expect(async () => {
                await axios.post("/api/v1/reviews", review_to_post, {headers:{'Content-Type': 'application/json','authorization': "not working"}})
            }).rejects.toThrowError("Request failed with status code 500");
        })
    
        test("Post review with db error (500 ERR)", async () => {
            db.createReview = jest.fn((review, user) => Promise.reject("Rejected promise"))
    
            return expect(async () => {
                await axios.post("/api/v1/reviews", review_to_post, {headers:{'Content-Type': 'application/json','authorization': adminuser}})
            }).rejects.toThrowError("Request failed with status code 500");
        })

    })

    describe("Update reviews", () => {
        /**
         * PUT "/review/:reviewId"
         * 100% coverage
         */
        test("Put review (200 OK)", async () => {
            db.editReview = jest.fn((review: Review, user: User) => {return new Promise<void>((resolve, reject) => resolve())})
    
            let response = await axios.put("/api/v1/reviews/1", review_to_post, { headers:{ 'Content-Type': 'application/json', 'authorization': adminuser}})
    
            expect(response.status).toEqual(200);
        })
    
        test("Put review without body (400 ERR)", async () => {
            return expect(async () => {
                await axios.put("/api/v1/reviews/1", {}, { headers:{ 'Content-Type': 'application/json', 'authorization': adminuser}})
            }).rejects.toThrowError("Request failed with status code 400");
        })
    
        test("Put review without authToken (401 ERR)", async () => {
            return expect(async () => {
                await axios.put("/api/v1/reviews/1", review_to_post, { headers:{ 'Content-Type': 'application/json', 'authorization': ""}})
            }).rejects.toThrowError("Request failed with status code 401");
        })
    
        test("Put review with wrangled authToken (500 ERR)", async () => {
            return expect(async () => {
                await axios.put("/api/v1/reviews/1", review_to_post, { headers:{ 'Content-Type': 'application/json', 'authorization': "not working"}})
            }).rejects.toThrowError("Request failed with status code 500");
        })
    
        test("Put review with db error (500 ERR)", async () => {
            db.editReview = jest.fn((review: Review, user: User) => {return new Promise<void>((resolve, reject) => reject("Promise rejected"))})
    
            return expect(async () => {
                await axios.put("/api/v1/reviews/1", review_to_post, { headers:{ 'Content-Type': 'application/json', 'authorization': adminuser}})
            }).rejects.toThrowError("Request failed with status code 500");
        })
    })

    describe("Delete reviews", () => {
        /**
         * DELETE "/review/:reviewId"
         * 100% coverage
         */
        test("succesfully delete review (200 OK)", async () => {
            db.deleteReview = jest.fn((id: number, user: User) => {return new Promise<void>((resolve, reject) => resolve())})
        
            let response = await axios.delete("api/v1/reviews/1", { headers:{ 'Content-Type': 'application/json', 'authorization': adminuser }})
            expect(response.status).toEqual(200);
        })
    
        test("delete review without authToken (401 ERR)", async () => {
            return expect(async () => {
                await axios.delete("/api/v1/reviews/1", { headers:{ 'Content-Type': 'application/json', 'authorization': ""}})
            }).rejects.toThrowError("Request failed with status code 401");
        })
    
        test("delete review with wrangled authToken (500 ERR)", async () => {
            return expect(async () => {
                await axios.delete("/api/v1/reviews/1", { headers:{ 'Content-Type': 'application/json', 'authorization': "not working"}})
            }).rejects.toThrowError("Request failed with status code 500");
        })
    
        test("delete review with db error (500 ERR)", async () => {
            db.deleteReview = jest.fn((id: number, user: User) => {return new Promise<void>((resolve, reject) => reject("Promise rejected"))})
    
            return expect(async () => {
                await axios.delete("/api/v1/reviews/1", { headers:{ 'Content-Type': 'application/json', 'authorization': adminuser}})
            }).rejects.toThrowError("Request failed with status code 500");
        })

    })
})

describe("ReviewVote requests", () => {
    describe("GET ReviewVotes", () => {
        /**
         * GET "/reviewVotes"
         * 100% coverage
         */
        test("Fetch all ReviewVotes (200 OK)", async () => {
            db.getAllReviewVotes = jest.fn(() => Promise.resolve(testReviewVotes));
    
            const response = await axios.get("/api/v1/reviewVotes");
    
            expect(response.status).toEqual(200);
            expect(response.data).toEqual(testReviewVotes);
        })

        test("Fetch all ReviewVotes (500 ERR)", async () => {
            db.getAllReviewVotes = jest.fn(() => Promise.reject(testReviewVotes));
    
            return expect(async () => {
                await axios.get("/api/v1/reviewVotes");
            }).rejects.toThrowError("Request failed with status code 500");
        })

        /**
         * GET /reviewVotes/:reviewId
         * 100% coverage
         */

        test("Fetch reviewVotes for review with id: 1 (200 OK)", async () => {
            db.getReviewVotesFromId = jest.fn((id: number) => Promise.resolve(testReviewVotes.filter(e => e.review_id == id)));
    
            const response = await axios.get("/api/v1/reviewVotes/1");
    
            expect(response.status).toEqual(200);
            expect(response.data).toEqual(testReviewVotes.filter(e => e.review_id == 1));
        })

        test("Fetch all ReviewVotes for review with id: 6 (404 ERR)", async () => {
            db.getReviewVotesFromId = jest.fn((id: number) => Promise.resolve(testReviewVotes.filter(e => e.review_id == id)));
    
            return expect(async () => {
                await axios.get("/api/v1/reviewVotes/6");
            }).rejects.toThrowError("Request failed with status code 404");
        })

        test("Fetch all ReviewVotes from review with id: 1 (500 ERR)", async () => {
            db.getReviewVotesFromId = jest.fn(() => Promise.reject("Promise rejected"));
    
            return expect(async () => {
                await axios.get("/api/v1/reviewVotes/1");
            }).rejects.toThrowError("Request failed with status code 500");
        })
    })

    describe("POST ReviewVotes", () => {
        /**
         * POST /reviewVotes/:reviewId/up
         */
        test("Upvote review with id: 1 (200 OK)", async () => {
            db.voteOnReview = jest.fn((id: number, vote: number, user: User) => Promise.resolve())
        
            let response = await axios.post("/api/v1/reviewVotes/1/up", {}, { headers:{ 'Content-Type': 'application/json', 'authorization': adminuser}});
            expect(response.status).toEqual(200);
        });

        test("Upvote review without authToken (401 ERR)", async () => {   
            return expect(async () => {
                await axios.post("/api/v1/reviewVotes/1/up", {}, {headers:{'Content-Type': 'application/json','authorization': ""}})
            }).rejects.toThrowError("Request failed with status code 401");
        })
    
        test("Upvote review with wrangled authToken (500 ERR)", async () => {
            return expect(async () => {
                await axios.post("/api/v1/reviewVotes/1/up", {}, {headers:{'Content-Type': 'application/json','authorization': "not working"}})
            }).rejects.toThrowError("Request failed with status code 500");
        })
    
        test("Upvote review with db error (500 ERR)", async () => {
            db.voteOnReview = jest.fn((id, vote, user) => Promise.reject("Rejected promise"))
    
            return expect(async () => {
                await axios.post("/api/v1/reviewVotes/1/up", {}, {headers:{'Content-Type': 'application/json','authorization': adminuser}})
            }).rejects.toThrowError("Request failed with status code 500");
        })

        test("Upvote review with duplicate entry (403 ERR)", async () => {
            db.voteOnReview = jest.fn((id, vote, user) => Promise.reject({"code": "ERR_DUP_ENTRY"}))
    
            return expect(async () => {
                await axios.post("/api/v1/reviewVotes/1/up", {}, {headers:{'Content-Type': 'application/json','authorization': adminuser}})
            }).rejects.toThrowError("Request failed with status code 403");
        })


        /**
         * POST /reviewVotes/:reviewId/down
         */
        test("downvote review with id: 1 (200 OK)", async () => {
            db.voteOnReview = jest.fn((id: number, vote: number, user: User) => Promise.resolve())
        
            let response = await axios.post("/api/v1/reviewVotes/1/down", {}, { headers:{ 'Content-Type': 'application/json', 'authorization': adminuser}});
            expect(response.status).toEqual(200);
        });

        test("downvote review without authToken (401 ERR)", async () => {   
            return expect(async () => {
                await axios.post("/api/v1/reviewVotes/1/down", {}, {headers:{'Content-Type': 'application/json','authorization': ""}})
            }).rejects.toThrowError("Request failed with status code 401");
        })
    
        test("Downvote review with wrangled authToken (500 ERR)", async () => {
            return expect(async () => {
                await axios.post("/api/v1/reviewVotes/1/down", {}, {headers:{'Content-Type': 'application/json','authorization': "not working"}})
            }).rejects.toThrowError("Request failed with status code 500");
        })
    
        test("Downvote review with db error (500 ERR)", async () => {
            db.voteOnReview = jest.fn((id, vote, user) => Promise.reject("Rejected promise"))
    
            return expect(async () => {
                await axios.post("/api/v1/reviewVotes/1/down", {}, {headers:{'Content-Type': 'application/json','authorization': adminuser}})
            }).rejects.toThrowError("Request failed with status code 500");
        })

        test("Downvote review with duplicate entry (403 ERR)", async () => {
            db.voteOnReview = jest.fn((id, vote, user) => Promise.reject({"code": "ERR_DUP_ENTRY"}))
    
            return expect(async () => {
                await axios.post("/api/v1/reviewVotes/1/down", {}, {headers:{'Content-Type': 'application/json','authorization': adminuser}})
            }).rejects.toThrowError("Request failed with status code 403");
        })
        
        /**
         * POST /reviewVotes/:reviewId/down
         */

    })

    describe("PUT reviewVotes", () => {
        /**
         * PUT "/reviewvotes/:reviewId"
         */
        test("Remove vote from review with id: 1 (200 OK)", async () => {
            db.removeVote = jest.fn((id: number) => Promise.resolve("success"))

            let response = await axios.put("/api/v1/reviewVotes/1", {}, { headers:{ 'Content-Type': 'application/json', 'authorization': adminuser}})
            expect(response.status).toEqual(200);
            expect(response.data).toEqual("YEAH! deleted vote from review with id: 1")
        })

        test("Remove vote from review with id: 6 (404 ERR)", async () => {
            db.removeVote = jest.fn((id:number) => Promise.resolve("no"))

            return expect(async () => {
                await axios.put("/api/v1/reviewVotes/1", {}, {headers:{'Content-Type': 'application/json','authorization': adminuser}})
            }).rejects.toThrowError("Request failed with status code 404");
        })

        test("Remove vote from review with id: 1 without authtoken (401 ERR)", async () => {
            return expect(async () => {
                await axios.put("/api/v1/reviewVotes/1", {}, {headers:{'Content-Type': 'application/json','authorization': ""}})
            }).rejects.toThrowError("Request failed with status code 401");
        })

        test("Remove vote from review with id: 1 with wrangled authtoken (500 ERR)", async () => {
            return expect(async () => {
                await axios.put("/api/v1/reviewVotes/1", {}, {headers:{'Content-Type': 'application/json','authorization': "not working"}})
            }).rejects.toThrowError("Request failed with status code 500");
        })

        test("Remove vote from review with id: 1 db error (500 ERR)", async () => {
            db.removeVote = jest.fn((id:number) => Promise.reject("no"))

            return expect(async () => {
                await axios.put("/api/v1/reviewVotes/1", {}, {headers:{'Content-Type': 'application/json','authorization': adminuser}})
            }).rejects.toThrowError("Request failed with status code 500");
        })
    })
})

describe("User requests", () => {

    /**
     * POST /login
     * 100% coverage
     */
    test("login and check if token can be assigned (200 OK)", async () => {
        db.login = jest.fn((user: User) => {
            return new Promise<string>((resolve, reject) => {
                resolve(generateToken(user))
            })
        })

        let response = await axios.post("/api/v1/login", testuser_to_login, {headers:{'Content-Type': 'application/json','authorization': ""}})
        
        expect(response.status).toEqual(200);
        expect(response.data).not.toEqual(undefined);
    })

    test("Login with db error (500 ERR)", async () => {
        db.login = jest.fn((user: User) => Promise.reject("Promise rejected"))
    
        return expect(async () => {
            await axios.post("/api/v1/login", testuser_to_login, {headers:{'Content-Type': 'application/json','authorization': ""}})
        }).rejects.toThrowError("Request failed with status code 500");
    })

    test("Login with wrong username/password combination (401 ERR)", async () => {
        db.login = jest.fn((user: User) => Promise.reject("Username or password does not match any known users in the database"))
    
        return expect(async () => {
            await axios.post("/api/v1/login", testuser_to_login, {headers:{'Content-Type': 'application/json','authorization': ""}})
        }).rejects.toThrowError("Request failed with status code 401");

    })

    test("Login without user data (400 ERR)", async () => {
        return expect(async () => {
            await axios.post("/api/v1/login", {}, {headers:{'Content-Type': 'application/json','authorization': ""}})
        }).rejects.toThrowError("Request failed with status code 400");

    })

    /**
     * POST /login
     * 100% coverage
     */
    test("Sign up new user (201 OK)", async () => {
        db.createUser = jest.fn((user: User) => {
            return new Promise<string>((resolve, reject) => {
                resolve("success")
            })
        })

        let response = await axios.post("/api/v1/signup", testuser_to_post, {headers:{'Content-Type': 'application/json','authorization': ""}})
        
        expect(response.status).toEqual(201);
        expect(response.data).toEqual("success! user: bob is now created!");
    })

    test("Sign up with db error (500 ERR)", async () => {
        db.createUser = jest.fn((user: User) => Promise.reject("Promise rejected"))
    
        return expect(async () => {
            await axios.post("/api/v1/signup", testuser_to_login, {headers:{'Content-Type': 'application/json','authorization': ""}})
        }).rejects.toThrowError("Request failed with status code 500");
    })

    test("Sign up with existing username/password combination (403 ERR)", async () => {
        db.createUser = jest.fn((user: User) => Promise.resolve("duplicate"))
    
        return expect(async () => {
            await axios.post("/api/v1/signup", testuser_to_login, {headers:{'Content-Type': 'application/json','authorization': ""}})
        }).rejects.toThrowError("Request failed with status code 403")  
    })

    test("Sign up without user data (400 ERR)", async () => {
        return expect(async () => {
            await axios.post("/api/v1/signup", {}, {headers:{'Content-Type': 'application/json','authorization': ""}})
        }).rejects.toThrowError("Request failed with status code 400");

    })

    /**
     * GET /user
     * 100% coverage
     */
    test("Get user id 200 (OK)", async () => {

        let results = await axios.get("/api/v1/user", {headers:{'Authorization': adminuser}})
        expect(results.data).toMatchObject({"user_id": 1})
    })

    test("Get user id without authtoken 401 (ERR)", async () => {
        return expect(async () => {
            await axios.get("/api/v1/user", {headers:{'authorization': ""}})
        }).rejects.toThrowError("Request failed with status code 401");
    })

    test("Get user id with wrangled authtoken 500 (ERR)", async () => {
        return expect(async () => {
            await axios.get("/api/v1/user", {headers:{'authorization': "not working"}})
        }).rejects.toThrowError("Request failed with status code 500");
    })
})

describe("Make sure hashing works", () => {
    test("Hash is unique", async () => {
        let hash1: string;
        await makeHash("password")
            .then(e => {
                hash1 = e;
            })
        await makeHash("password")
            .then(hash2 => {
                expect(hash2).not.toEqual(hash1)
            })
    })
})
